# ScriptLauncher

## Synopsis

SkunkWorks script to launch other scripts based on which character is logged in.

## Installation
- Install Decal. [decaldev.com](https://www.decaldev.com)
- Install SkunkWorks for Asheron's Call. [SkunkWorks35-500.exe](https://sourceforge.net/projects/skunkworks/files/SkunkWorks/3.5/SkunkWorks35-500.exe/download)
- Download updated SkunkWorks skapi.dll file. [SkunkWorks3.5.509.zip](https://sourceforge.net/projects/skunkworks/files/SkunkWorks/3.5/SkunkWorks3.5.509.zip)
- Extract SkunkWorks3.5.509.zip to your SkunkWorks directory, overwriting the existing skapi.dll file.
- Download [ScriptLauncher](https://gitlab.com/Cyprias/ScriptLauncher/tags) and extract it to your SkunkWorks directory. 
- Set SkunkWorks to load this script by default. (Configure > Login Script)
- Edit the ScriptLauncher.js file to include your characters

## License
MIT License	(http://opensource.org/licenses/MIT)