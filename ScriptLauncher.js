/********************************************************************************************\
	Script:         ScriptLauncher-1.0
	Purpose:        Launch different scripts based on what character is in use.
	Author:         Cyprias
	Date:           2018/07/27
	Licence:        MIT License	(http://opensource.org/licenses/MIT)
\*********************************************************************************************/

var MAJOR = "ScriptLauncher-1.0";
var MINOR = 190318; // Year Month Day

var core = {};
var _c = core.config = {};

/*******************\
	Configuration
\*******************/

// Assign what script to load per character name.
// _c.characters["Server Name"]["Character Name"] = "C:/Path/To/Script.swx";
_c.characters = {};
_c.characters["Reefcull"] = {};
_c.characters["Reefcull"]["Barbie"] = "D:/Games/SkunkWorks/BuffBot/BuffBot.swx";

core.chat = function chat() {
	var args = Array.prototype.slice.call(arguments);
	skapi.OutputLine("[SL] " + args.join(', '), opmChatWnd);
};

function main() {
	if (skapi.plig == pligNotRunning) {
		core.chat("Asheron's call isn't running. Cannot launch script.");
		return console.StopScript();
	} else if (skapi.plig == pligAtLogin) {
		core.chat("A character hasn't logged in yet. Cannot launch script.");
		return console.StopScript();
	}

	var szScript = core.config.characters[skapi.szWorld] && core.config.characters[skapi.szWorld][skapi.acoChar.szName];
	if (szScript) {
		core.chat("Loading " + szScript + "...");
		console.RunScript(szScript);
	} else {
		core.chat("No script selected for " + skapi.szWorld + " / " + skapi.acoChar.szName + ". Shutting down...");
	}
	console.StopScript(); // Prevents Skunkworks from sometimes crashing.
};